package nl.han.ica.ddoa.dda.sortServer;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import nl.han.ica.ddoa.dda.interfaces.ISortFactory;
import nl.han.ica.ddoa.dda.interfaces.ISorter;
import nl.han.ica.ddoa.dda.sorter.sorters.BubbleSorter;
import nl.han.ica.ddoa.dda.sorter.sorters.SelectionSorter;

public final class SortFactory extends UnicastRemoteObject implements ISortFactory{

	/**
	 * 
	 */
	private static SortFactory sortFactory;
	private static final long serialVersionUID = 1L;

	private SortFactory() throws RemoteException {
		super();
	}
	
	/**
	 * A Singelton implementation 
	 * Returns an existing sort factory or creates one
	 * @return
	 */
	public static synchronized SortFactory getSortFactory(){
		if(sortFactory == null){
			try {
				sortFactory = new SortFactory();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		return sortFactory;
	}

	@Override
	public ISorter createSorter() throws RemoteException {
		ISorter sorter;
		switch((int)( 2 * Math.random())){
			case 0:
				sorter = new BubbleSorter();
				break;
			case 1:
				sorter = new SelectionSorter();
				break;
			default:
				sorter = new BubbleSorter();
				break;
		}
		return sorter;
	}
}
