package nl.han.ica.ddoa.dda.sortServer;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import nl.han.ica.ddoa.dda.interfaces.ISortFactory;

public final class SortServer {
	public static final int RMI_SERVER_PORT = 1099;
	
	private SortServer(){}
	/**
	 * Creates the sortFactory
	 * @param args
	 * @throws RemoteException
	 */
	public static void main(final String[] args) throws RemoteException {
		Registry registry = null;
		try { // special exception handler for registry creation
			registry = LocateRegistry.createRegistry(RMI_SERVER_PORT);
			System.out.println("java RMI registry created.");
		} catch (RemoteException e) {
			// do nothing, error means registry already exists
			registry = LocateRegistry.getRegistry(RMI_SERVER_PORT);
			System.out.println("java RMI registry already exists.");
		}
		ISortFactory sortFactory = SortFactory.getSortFactory();
		try {
			registry.bind("SortServer", sortFactory);
		} catch (Exception e) {
			registry.rebind("SortServer", sortFactory);
		}
	}
}
