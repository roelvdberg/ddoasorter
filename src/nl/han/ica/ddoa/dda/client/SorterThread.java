package nl.han.ica.ddoa.dda.client;

import java.rmi.Naming;
import java.util.Observable;	

import nl.han.ica.ddoa.dda.interfaces.ISortFactory;
import nl.han.ica.ddoa.dda.interfaces.ISorter;

public class SorterThread 
	extends Observable 
	implements Runnable 
{
	/**
	 * The number of this thread.
	 */
	private int threadNumber;
	
	/**
	 * The list to be sorted in this thread.
	 */
	private Comparable[] list;
	
	/**
	 * Creates a new sorter and puts him in his own thread after sorting a variable he notifies his observer
	 * @param list
	 * @param threadNumber
	 */
	public SorterThread(final Comparable[] list, final int threadNumber){
		this.list = list;
		this.threadNumber = threadNumber;
		this.addObserver(ListJoiner.getListJoiner());
		
		Thread sorter = new Thread(this);
		sorter.setDaemon(false);
		sorter.start();
	}
	
	/**
	 * Contacts the SortFactory and sorts an list
	 */
	@Override
	public final void run() {
		try {
			ISortFactory sortFactory = (ISortFactory) Naming.lookup("SortServer");
			ISorter sorter = sortFactory.createSorter();
			list = sorter.sort(list);
			setChanged();
			notifyObservers(this);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * returns the current thread number
	 * @return
	 */
	public final int getThreadNumber(){
		return threadNumber;
	}
	
	/**
	 * returns the list, This method doesn't guarantee that the list is sorted.
	 * This list is sorted once
	 * @return
	 */
	public final Comparable[] getList(){
		return list;
	}
}
