package nl.han.ica.ddoa.dda.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;

public final class ListJoiner implements Observer {
	private int numberOfListsAdded = 0;
	private int totalNumberOfLists = 0;
	private Comparable[][] unjoinedList;
	private Comparable[] joinedList;
	
	private static ListJoiner listJoiner;

	private ListJoiner(final int totalNumberOfLists) {
		this.totalNumberOfLists = totalNumberOfLists;
		unjoinedList = new Comparable[totalNumberOfLists][];
	}
	
	/**
	 * Singleton, creates a new instantiation of listjoiner if the instantiation doesn't exists. 
	 * if there exists an instantiation of listjoiner it will return that instantiation 
	 * @param totalNumberOfLists the total number of lists
	 * @return
	 */
	public static ListJoiner getListJoiner(final int totalNumberOfLists){
		if(listJoiner == null){
			listJoiner = new ListJoiner(totalNumberOfLists);
		}
		return listJoiner;
	}

	
	/**
	 * Returns the instantiation of listJoiner 
	 * @return 
	 */
	public static ListJoiner getListJoiner(){
		return listJoiner;
	}
	
	/**
	 * Used by the Observer Observable pattern. It accepts an instance of SorterThread.
	 */
	@Override
	public void update(final Observable o, final Object arg) {
		if (o instanceof SorterThread) {
			addListPart(
					((SorterThread) arg).getList(),
					((SorterThread) arg).getThreadNumber()
				);

			if (numberOfListsAdded == totalNumberOfLists) {
				join();
				printJoinedList();
			}
		}
	}

	/**
	 * Adds a result of an sorter.
	 * @param list
	 * @param threadNumber
	 */
	public void addListPart(final Comparable[] list, final int threadNumber) {
		unjoinedList[threadNumber] = list;
		numberOfListsAdded++;
	}

	/**
	 * Joins the results to one list
	 */
	@SuppressWarnings("unchecked")
	public void join() {
		if(numberOfListsAdded == totalNumberOfLists){
			ArrayList result = new ArrayList();
			for (int i = 0; i < unjoinedList.length; i++) {
				result.addAll(Arrays.asList(unjoinedList[i]));
			}
			this.joinedList = new Comparable[result.size()];
			for (int i = 0; i < result.size(); i++) {
				this.joinedList[i] = (Comparable) result.get(i);
			}
		}
	}

	/**
	 * Prints the current list
	 */
	private void printJoinedList() {
		for (int i = 0; i < joinedList.length; i++) {
			System.out.println(joinedList[i]);
		}
		System.out.println("============");
		System.out.println(joinedList.length);
	}
	
	/**
	 * gets the current number of lists
	 * @return
	 */
	public int getNumberOfListsAdded(){
		return numberOfListsAdded;
	}
	
	/**
	 * The result is null if the list is not yet joined
	 * @return
	 */
	public Comparable[] getJoinedList(){
		return joinedList;
	}
	
	/**
	 * Only for unit testing
	 */
	public void destroy(){
		listJoiner = null;
	}
}
