package nl.han.ica.ddoa.dda.client;

import java.util.ArrayList;
import java.util.Arrays;

import nl.han.ica.ddoa.dda.comparables.Screw;
import nl.han.ica.ddoa.dda.interfaces.IDecimalRepresentation;

public class SortClient {
	private int numberOfLists;

	/**
	 * Start the sortClient
	 * @param args
	 */
	public static void main(final String[] args) {
		SortClient sortClient = new SortClient(4);
		sortClient.sort(100, Screw.class);
	}

	/**
	 * Creates an instance of SortClient
	 * @param numberOfLists
	 */
	public SortClient(final int numberOfLists){
		this.numberOfLists = numberOfLists;
	}
	
	/**
	 * Sorts a random list with the specified type and length
	 * @param size
	 * @param type
	 */
	@SuppressWarnings({ "rawtypes" })
	public final void sort(final int size, final Class type) {
		Comparable[] list = generateRandomList(size, type);

		Comparable[][] lists = splice(list);
		ListJoiner.getListJoiner(lists.length);
		for (int i = 0; i < lists.length; i++) {
			new SorterThread(lists[i], i);
		}
	}

	/**
	 * Generates a Random List of Integers
	 * @param size
	 * @param type
	 */
	@SuppressWarnings({ "unchecked" })
	private Comparable<Integer>[] generateRandomIntegers(final int length) {
		Comparable<Integer>[] unSortedIntergers = new Comparable[length];
		for (int i = 0; i < length; i++) {
			unSortedIntergers[i] = (int) (length * Math.random());
		}
		return unSortedIntergers;
	}
	
	/**
	 * Generates a Random List of Strings
	 * @param size
	 * @param type
	 */
	@SuppressWarnings({ "unchecked" })
	private Comparable<String>[] generateRandomStrings(final int length) {
		Comparable<String>[] unSortedStrings = new Comparable[length];
		for (int i = 0; i < length; i++) {
			int stringLength = (int) (30 * Math.random() + 1);
			String value = "";
			for (int j = 0; j < stringLength; j++) {
				value += (char) (int) ((Math.random() * 26) + 65);
			}
			unSortedStrings[i] = value;
		}
		return unSortedStrings;
	}

	/**
	 * Generates a Random List of Screws
	 * @param size
	 * @param type
	 */
	@SuppressWarnings({ "unchecked" })
	private Comparable<Screw>[] generateRandomScrews(final int length) {
		Comparable<Screw>[] comparable = new Comparable[length];
		for (int i = 0; i < length; i++) {
			comparable[i] = new Screw();
		}
		return comparable;
	}
	
	/**
	 * Generates a Random List of the specified type
	 * @param size
	 * @param type
	 */
	@SuppressWarnings({ "rawtypes" })
	private Comparable[] generateRandomList(final int length, final Class type) {
		Comparable[] comparable = new Comparable[length];
		if (type == Integer.class) {
			comparable = generateRandomIntegers(length);
		} else if (type == String.class) {
			comparable = generateRandomStrings(length);
		} else if (type == Screw.class) {
			comparable = generateRandomScrews(length);
		}
		return comparable;
	}

	/**
	 * Splices an list in the specified numbers of parts mostly used for testing
	 * @param list
	 * @param numberOfLists
	 * @return
	 */
	public final Comparable[][] spliceList(final Comparable[] list,
			final int numberOfLists) {
		this.numberOfLists = numberOfLists;
		return splice(list);
	}

	/**
	 * Splices an list into the parts specified in the field numberOfListsa
	 * @param list
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public final Comparable[][] splice(final Comparable[] list) {
		ArrayList<Comparable>[] splicedList = new ArrayList[numberOfLists];

		ArrayList<Comparable> itemList = new ArrayList();
		itemList.addAll(Arrays.asList(list));

		/** split all **/
		for (int i = 0; i < numberOfLists; i++) {
			splicedList[i] = new ArrayList<Comparable>();
			for (int j = 0; j < itemList.size(); j++) {
				if (checkIfItemBelongsInList(list, itemList, i, j)) {
					splicedList[i].add(itemList.get(j));
					itemList.remove(j);
					j--;
				}
			}
		}
		return arrayListToArray(splicedList);
	}

	private boolean checkIfItemBelongsInList(final Comparable[] list,
			final ArrayList<Comparable> itemList,final int i, final int j) {
		return getDecimalRepresentation(itemList.get(j)) <= (getMinValueInList(list) + (getSizeOfSteps(
				numberOfLists, list) * (i + 1)));
	}

	/**
	 * Used to calculate the DecimalRepresentation, this is possible for integers, Strings and objects implementing the IDecimalRepresentation Interface
	 * @param value
	 * @return
	 */
	@SuppressWarnings({ "rawtypes" })
	private int getDecimalRepresentation(final Comparable value) {
		if (value instanceof Integer) {
			return (Integer) value;
		} else if (value instanceof String) {
			return ((String) value).charAt(0);
		} else if (value instanceof IDecimalRepresentation) {
			return ((IDecimalRepresentation) value).getDecimalRepresentation();
		}
		return 0;
	}

	/**
	 * Calculates the size of every step in spliceList
	 * @param numberOfParts
	 * @param list
	 * @return
	 */
	public final int getSizeOfSteps(final int numberOfParts,
			final Comparable[] list) {
		return (int) Math
				.ceil((double) (getMaxValueInList(list) - getMinValueInList(list))
						/ (double) numberOfParts);
	}

	/**
	 * Returns the maximum value in a list
	 * @param list
	 * @return
	 */
	@SuppressWarnings({ "rawtypes" })
	public final int getMaxValueInList(final Comparable[] list) {
		if (list.length == 0) {
			return 0;
		}
		int maxValue = getDecimalRepresentation(list[0]);
		for (int i = 1; i < list.length; i++) {
			if (maxValue < getDecimalRepresentation(list[i])) {
				maxValue = getDecimalRepresentation(list[i]);
			}
		}
		return maxValue;
	}

	/**
	 * Returns the minimum value in a list
	 * @param list
	 * @return
	 */
	@SuppressWarnings({ "rawtypes" })
	public final int getMinValueInList(final Comparable[] list) {
		if (list.length == 0) {
			return 0;
		}
		int minValue = getDecimalRepresentation(list[0]);
		for (int i = 0; i < list.length; i++) {
			if (minValue > getDecimalRepresentation(list[i])) {
				minValue = getDecimalRepresentation(list[i]);
			}
		}
		return minValue;
	}

	/**
	 * Changes an array with ArrayLists to a multidimensional array
	 * @param splicedList
	 * @return
	 */
	@SuppressWarnings({ "rawtypes" })
	private Comparable[][] arrayListToArray(
			final ArrayList<Comparable>[] splicedList) {
		Comparable[][] returnList = new Comparable[splicedList.length][];
		for (int i = 0; i < splicedList.length; i++) {
			returnList[i] = splicedList[i]
					.toArray(new Comparable[splicedList[i].size()]);
		}
		return returnList;
	}
}
