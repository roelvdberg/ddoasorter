package nl.han.ica.ddoa.dda.comparables;

import java.io.Serializable;

import nl.han.ica.ddoa.dda.interfaces.IDecimalRepresentation;

public class Screw 
	implements Comparable, Serializable, IDecimalRepresentation 
{
	private int length;
	private String screwHead;
	
	/**
	 * Creates a Screw with random values
	 */
	public Screw(){
		this.length = generateRandomLength(5, 25);
		this.screwHead = generateRandomScrewHead();
	}

	/**
	 * Creates a screw with the specified values
	 * @param length
	 * @param screwHead
	 */
	public Screw(final int length, final String screwHead){
		this.length = length;
		this.screwHead = screwHead;
	}
	
	/**
	 * Generates a random screwHead
	 * @return
	 */
	private String generateRandomScrewHead() {
		String[] screwHead = {"Slot", "Phillips", "Torx", "Hex"};
		return screwHead[((int) (Math.random() * screwHead.length))];
	}

	/**
	 * generates a random screwLength according to the min and maxLength
	 * @param minLength
	 * @param maxLength
	 * @return
	 */
	private int generateRandomLength(final int minLength, final int maxLength) {
		int maxSize = maxLength - minLength;
		return (int)(Math.random()* maxSize) + minLength;
	}

	@Override
	public final int compareTo(final Object o) {
		if(o instanceof Screw){
			Screw screw = (Screw) o;
			if(isSameScrewHead(screw)){
				return isSameLength(screw);
			}else{
				return this.screwHead.compareTo(screw.getScrewHead());
			}
		}
		return -1;
	}

	/**
	 * checks if the given screw has the same head as the current screw
	 * @param screw
	 * @return
	 */
	private boolean isSameScrewHead(final Screw screw) {
		return this.screwHead.compareTo(screw.getScrewHead()) == 0;
	}


	/**
	 * checks if the given screw has the same head as the current screw
	 * @param screw
	 * @return
	 */
	private int isSameLength(final Screw screw) {
		if(this.length<screw.getLength()){
			return -1;
		} else if(this.length == screw.getLength()){
			return 0;
		} else{
			return 1;
		}
	}
	
	/**
	 * Returns the the data inside this object as an string
	 */
	public final String toString(){
		return this.screwHead + " - " + this.length;
	}
	
	/**
	 * Gets the length of the screw.
	 * @return The length of the screw.
	 */
	public final int getLength(){
		return this.length;
	}
	
	/**
	 * Gets the type of screw head of the screw.
	 * @return The type of screw head on the screw.
	 */
	public final String getScrewHead(){
		return this.screwHead;
	}

	/**
	 * Return the descimal value of a screw.
	 * @return The Decimal value of a screw.
	 */
	@Override
	public final int getDecimalRepresentation() {
		int value = 0;
		value += screwHead.charAt(0)*1000;
		value += length;
		return value;
	}
}
