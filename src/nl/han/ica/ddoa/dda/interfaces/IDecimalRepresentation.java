/**
 * 
 * @author Roel van den Berg
 */

package nl.han.ica.ddoa.dda.interfaces;

public interface IDecimalRepresentation {
	/**
	 * Returns an Integer value representing the state of the object.
	 * @return The decimal representation of an object.
	 */
	public int getDecimalRepresentation();
}
