package nl.han.ica.ddoa.dda.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ISortFactory extends Remote{
	/**
	 * Creates a sorter
	 * @return
	 * @throws RemoteException
	 */
	ISorter createSorter() throws RemoteException;
}
