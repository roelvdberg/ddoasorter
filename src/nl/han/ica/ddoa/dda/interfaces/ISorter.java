package nl.han.ica.ddoa.dda.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ISorter extends Remote{

	/**
	 * Sorts the variable comparable and returns it
	 * @param comparable
	 * @return Sorted version of the param comparable
	 * @throws RemoteException
	 */
	Comparable[] sort(Comparable[] comparable) throws RemoteException;
}
