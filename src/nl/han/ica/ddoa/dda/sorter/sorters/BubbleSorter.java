package nl.han.ica.ddoa.dda.sorter.sorters;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import nl.han.ica.ddoa.dda.interfaces.ISorter;

public class BubbleSorter extends UnicastRemoteObject implements ISorter {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BubbleSorter() throws RemoteException {
		super();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public final Comparable[] sort(final Comparable[] comparable) throws RemoteException {
		for(int j = 0; j < comparable.length; j++){
			for(int i = 1; i < comparable.length; i++){
				if(comparable[i-1].compareTo(comparable[i]) > 0 ){
					Comparable current = comparable[i];
					comparable[i] = comparable[i-1];
					comparable[i-1] = current;
				}
			}
		}
		return comparable;
	}

}
