package nl.han.ica.ddoa.dda.sorter.sorters;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import nl.han.ica.ddoa.dda.interfaces.ISorter;

public class SelectionSorter extends UnicastRemoteObject implements ISorter {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public SelectionSorter() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	public final Comparable[] sort(final Comparable[] comparable) throws RemoteException{
		for(int i = 0; i < comparable.length; i++){
			int replacementIndex = i;
			for(int j = i; j < comparable.length; j++){
				if(comparable[replacementIndex].compareTo(comparable[j])> 0){
					replacementIndex = j;
				}
			}
			Comparable temp = comparable[i];
			comparable[i] = comparable[replacementIndex];
			comparable[replacementIndex] = temp;
		}
		return comparable;
	}

}
