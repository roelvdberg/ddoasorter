package nl.han.ica.ddoa.dda.sorterTest;

import static org.junit.Assert.*;

import nl.han.ica.ddoa.dda.comparables.Screw;

import org.junit.Before;
import org.junit.Test;

public class ScrewTest {
	private Screw screw;

	@Before
	public void tearUp() {
		screw = new Screw(10, "Slot");
	}

	@Test
	public void sameSort() {
		Screw screw1 = new Screw(10, "Slot");
		assertEquals(0, screw.compareTo(screw1));
	}

	@Test
	public void typeGreater() {
		Screw screw1 = new Screw(10, "Phillips");
		assertTrue(0 < screw.compareTo(screw1));
	}

	@Test
	public void typeSmaller() {
		Screw screw1 = new Screw(10, "Torx");
		assertTrue(0 > screw.compareTo(screw1));
	}

	@Test
	public void lengthGreater() {
		Screw screw1 = new Screw(9, "Slot");
		assertTrue(0 < screw.compareTo(screw1));
	}

	@Test
	public void lengthSmaller() {
		Screw screw1 = new Screw(11, "Slot");
		assertTrue(0 > screw.compareTo(screw1));
	}
}
