package nl.han.ica.ddoa.dda.sorterTest;

import static org.junit.Assert.*;

import nl.han.ica.ddoa.dda.client.SortClient;
import nl.han.ica.ddoa.dda.comparables.Screw;

import org.junit.Before;
import org.junit.Test;

public class SortClientTest {
	private SortClient sortClient;
	@Before
	public void tearUp(){
		sortClient = new SortClient(2);
	}
	
	@Test
	public void spliceIntegerTest(){
		Comparable[] list = {0,1,2,3,4,5};
		Comparable[][] expectedReturnList = new Comparable[2][];
		expectedReturnList[0] = new Comparable[4];
		expectedReturnList[1] = new Comparable[2];
		expectedReturnList[0][0] = 0;
		expectedReturnList[0][1] = 1;
		expectedReturnList[0][2] = 2; 
		expectedReturnList[0][3] = 3;
		expectedReturnList[1][0] = 4;
		expectedReturnList[1][1] = 5;
				
		assertArrayEquals(expectedReturnList, sortClient.spliceList(list, 2));
	}
	
	@Test
	public void spliceStringTest(){
		Comparable[] list = {"a", "b", "c", "d", "e", "f"};
		Comparable[][] expectedReturnList = new Comparable[2][];
		expectedReturnList[0] = new Comparable[4];
		expectedReturnList[1] = new Comparable[2];
		expectedReturnList[0][0] = "a";
		expectedReturnList[0][1] = "b";
		expectedReturnList[0][2] = "c"; 
		expectedReturnList[0][3] = "d";
		expectedReturnList[1][0] = "e";
		expectedReturnList[1][1] = "f"; 
				
		assertArrayEquals(expectedReturnList, sortClient.spliceList(list, 2));
	}
	
	/* maxValueInList tests */
	@Test
	public void maxValueInListEmptyList(){
		assertEquals(0, sortClient.getMaxValueInList(getEmptyList()));
		
	}

	@Test
	public void maxValueInListOneIntegerItem(){
		assertEquals(1, sortClient.getMaxValueInList(getIntegerListOneItem()));
	}

	@Test
	public void maxValueInListMultipleIntegerItems(){
		assertEquals(98, sortClient.getMaxValueInList(getIntegerListMultipleItems()));
		
	}

	@Test
	public void maxValueInListOneStringItem(){
		assertEquals(97, sortClient.getMaxValueInList(getStringListOneItem()));
	}

	@Test
	public void maxValueInListMultipleStringItems(){
		assertEquals(101, sortClient.getMaxValueInList(getStringListMultipleItems()));
	}

	@Test
	public void maxValueInListOneScrewItem(){
		assertEquals(83010, sortClient.getMaxValueInList(getScrewListOneItem()));
	}
	
	@Test
	public void maxValueInListMultipleScrewItems(){
		assertEquals(83018, sortClient.getMaxValueInList(getScrewListMultipleItems()));
	}

	/* minValueInList tests */
	public void minValueInListEmptyList(){
		assertEquals(0, sortClient.getMinValueInList(getEmptyList()));
		
	}
	
	@Test
	public void minValueInListOneIntegerItem(){
		assertEquals(1, sortClient.getMinValueInList(getIntegerListOneItem()));
	}
	
	@Test
	public void minValueInListMultipleIntegerItems(){
		assertEquals(1, sortClient.getMinValueInList(getIntegerListMultipleItems()));
		
	}
	
	@Test
	public void minValueInListOneStringItem(){
		assertEquals(97, sortClient.getMinValueInList(getStringListOneItem()));
	}
	
	@Test
	public void minValueInListMultipleStringItems(){
		assertEquals(97, sortClient.getMinValueInList(getStringListMultipleItems()));
	}

	@Test
	public void minValueInListOneScrewItem(){
		assertEquals(83010, sortClient.getMinValueInList(getScrewListOneItem()));
	}
	
	@Test
	public void minValueInListMultipleScrewItems(){
		assertEquals(72010, sortClient.getMinValueInList(getScrewListMultipleItems()));
	}

	/* getSizeOfSteps tests */
	@Test
	public void getSizeOfStepsEmptyList(){
		assertEquals(0, sortClient.getSizeOfSteps(2, getEmptyList()));
	}
	
	@Test
	public void getSizeOfStepsOneIntegerList(){
		assertEquals(0, sortClient.getSizeOfSteps(2, getIntegerListOneItem()));
	}
	
	@Test
	public void getSizeOfStepsMultipleIntegerList(){
		assertEquals(49, sortClient.getSizeOfSteps(2, getIntegerListMultipleItems()));
	}
	
	@Test
	public void getSizeOfStepsOneStringList(){
		assertEquals(0, sortClient.getSizeOfSteps(2, getStringListOneItem()));
	}
	
	@Test
	public void getSizeOfStepsMultipleStringList(){
		assertEquals(2, sortClient.getSizeOfSteps(2, getStringListMultipleItems()));
	}
	
	@Test
	public void getSizeOfStepsOneScrewList(){
		assertEquals(0, sortClient.getSizeOfSteps(2, getScrewListOneItem()));
	}
	
	@Test
	public void getSizeOfStepsMultipleScrewList(){
		assertEquals(5504, sortClient.getSizeOfSteps(2, getScrewListMultipleItems()));
	}

	
	/* Methods to create Lists */
	private Comparable[] getEmptyList() {
		Comparable[] list = new Comparable[0];
		return list;
	}

	private Comparable[] getIntegerListOneItem() {
		Comparable[] list = new Comparable[1];
		list[0] = 1;
		return list;
	}

	private Comparable[] getIntegerListMultipleItems() {
		Comparable[] list = {1,10,79,45,86,23,98,1,9};
		return list;
	}

	private Comparable[] getStringListOneItem() {
		Comparable[] list = new Comparable[1];
		list[0] = "a";
		return list;
	}
	
	private Comparable[] getStringListMultipleItems() {
		Comparable[] list = {"b","e","a","d","c"};
		return list;
	}

	private Comparable[] getScrewListOneItem() {
		Comparable[] list = new Comparable[1];
		list[0] = new Screw(10,"Slot");
		return list;
	}

	private Comparable[] getScrewListMultipleItems() {
		Comparable[] list = {
								new Screw(10,"Slot"),
								new Screw(15,"Slot"),
								new Screw(18,"Slot"),
								new Screw(10,"Phillips"),
								new Screw(10,"Hex")
							};
		return list;
	}
}
