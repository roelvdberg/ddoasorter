package nl.han.ica.ddoa.dda.sorterTest;

import static org.junit.Assert.*;

import nl.han.ica.ddoa.dda.client.ListJoiner;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ListJoinerTest {
	ListJoiner listJoiner;
	@Before
	public void tearUp(){
		listJoiner = ListJoiner.getListJoiner(2);
	}
	
	@After
	public void teadDown(){
		listJoiner.destroy();
	}
	
	@Test
	public void singleTonTest() {
		assertEquals(listJoiner, ListJoiner.getListJoiner());
	}

	@Test
	public void addListPartCountingZeroItemsTest(){
		assertEquals(0, listJoiner.getNumberOfListsAdded());
	}

	@Test
	public void addListPartCountingOneItemTest(){
		listJoiner.addListPart(getListPartOne(), 0);
		assertEquals(1, listJoiner.getNumberOfListsAdded());
	}
	
	@Test
	public void addListPartCountingTwoItemsTest(){
		listJoiner.addListPart(getListPartOne(), 0);
		listJoiner.addListPart(getListPartTwo(), 1);
		assertEquals(2, listJoiner.getNumberOfListsAdded());
	}
	
	@Test
	public void joinEmptyListTest(){
		listJoiner.join();
		assertArrayEquals(null, listJoiner.getJoinedList());
	}
	
	@Test
	public void joinOneItemOfTwoTest(){
		listJoiner.addListPart(getListPartOne(), 0);
		listJoiner.join();
		assertArrayEquals(null, listJoiner.getJoinedList());
	}
	
	@Test
	public void joinTwoItemsOfTwoTest(){
		listJoiner.addListPart(getListPartOne(), 0);
		listJoiner.addListPart(getListPartTwo(), 1);
		listJoiner.join();
		assertArrayEquals(getCombinedList(), listJoiner.getJoinedList());
	}
	
	private Comparable[] getListPartOne(){
		Comparable[] listOne = {0,1,2,3};
		return listOne;
	}
	
	private Comparable[] getListPartTwo(){
		Comparable[] listTwo = {4,5,6,7,8,9,10};
		return listTwo;
	}
	
	private Comparable[] getCombinedList(){
		Comparable[] combinedList = {0,1,2,3,4,5,6,7,8,9,10};
		return combinedList;
	}

}
