package nl.han.ica.ddoa.dda.sorterTest;

import static org.junit.Assert.*;

import java.rmi.RemoteException;

import nl.han.ica.ddoa.dda.comparables.Screw;
import nl.han.ica.ddoa.dda.interfaces.ISorter;
import nl.han.ica.ddoa.dda.sorter.sorters.SelectionSorter;

import org.junit.Before;
import org.junit.Test;

public class SelectionSorterTest {
	private ISorter sorter;

	@Before
	public void tearUp() {
		try {
			this.sorter = new SelectionSorter();
		} catch (Exception e) {

		}
	}

	@Test
	public void sortEmptyListTest() throws RemoteException {
		Comparable[] emptyList = {};

		assertArrayEquals(emptyList, sorter.sort(emptyList));
	}

	@Test
	public void sortEmptyOneIntegerTest() throws RemoteException {
		Comparable[] oneItemList = { 1 };

		assertArrayEquals(oneItemList, sorter.sort(oneItemList));
	}

	@Test
	public void sortEmptySortedIntegerTest() throws RemoteException {
		Comparable[] sortedList = { 1, 2, 3 };

		assertArrayEquals(sortedList, sorter.sort(sortedList));
	}

	@Test
	public void sortEmptyUnsortedIntegerTest() throws RemoteException {
		Comparable[] sortedList = { 1, 2, 3 };
		Comparable[] unsortedList = { 1, 3, 2 };

		assertArrayEquals(sortedList, sorter.sort(unsortedList));
	}

	@Test
	public void sortEmptyOneStringTest() throws RemoteException {
		Comparable[] oneItemList = { "a" };

		assertArrayEquals(oneItemList, sorter.sort(oneItemList));
	}

	@Test
	public void sortEmptySortedStringTest() throws RemoteException {
		Comparable[] sortedList = { "a", "b", "c" };

		assertArrayEquals(sortedList, sorter.sort(sortedList));
	}

	@Test
	public void sortEmptyUnsortedStringTest() throws RemoteException {
		Comparable[] sortedList = { "a", "b", "c" };
		Comparable[] unsortedList = { "c", "a", "b" };

		assertArrayEquals(sortedList, sorter.sort(unsortedList));
	}

	@Test
	public void sortEmptyOneScrewTest() throws RemoteException {
		Comparable[] oneItemList = { new Screw(10, "Phillips") };

		assertArrayEquals(oneItemList, sorter.sort(oneItemList));
	}

	@Test
	public void sortEmptySortedScrewTest() throws RemoteException {
		Comparable[] sortedList = { new Screw(10, "Phillips"), new Screw(15, "Phillips"), new Screw(20, "Phillips") };

		assertArrayEquals(sortedList, sorter.sort(sortedList));
	}

	@Test
	public void sortEmptyUnsortedScrewTest() throws RemoteException {
		Comparable[] sortedList = { new Screw(10, "Phillips"), new Screw(15, "Phillips"), new Screw(20, "Phillips") };
		Comparable[] unsortedList = { sortedList[0], sortedList[2], sortedList[1] };

		assertArrayEquals(sortedList, sorter.sort(unsortedList));
	}
}
