package nl.han.ica.ddoa.dda.sorterTest;

import static org.junit.Assert.*;

import java.rmi.RemoteException;

import nl.han.ica.ddoa.dda.interfaces.ISorter;
import nl.han.ica.ddoa.dda.sorter.sorters.BubbleSorter;
import nl.han.ica.ddoa.dda.sorter.sorters.SelectionSorter;

import org.junit.Ignore;
import org.junit.Test;

@Ignore("Test for the last assignment")
public class SorterPreformanceTest {
	private int sortTimes = 50000;
	
	@Test
	public void preformenceBubbleSorterTest() throws RemoteException{
		ISorter sorter = new BubbleSorter();
		Comparable[] list = {100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199};
		long startTime = System.currentTimeMillis();
		for(int i = 0; i < sortTimes; i++){
			sorter.sort(list);
		}
		long finishTime = System.currentTimeMillis();
		
		System.out.println("BubbleSorter took: "+(finishTime-startTime)+ " ms");
	}

	@Test
	public void preformenceBubbleSorter2xTest() throws RemoteException{
		ISorter sorter = new BubbleSorter();
		Comparable[] list = {100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199};
		long startTime = System.currentTimeMillis();
		for(int i = 0; i < sortTimes; i++){
			sorter.sort(list);
		}
		long finishTime = System.currentTimeMillis();
		
		System.out.println("BubbleSorter took 2x: "+(finishTime-startTime)+ " ms");
	}

	@Test
	public void preformenceBubbleSorter4xTest() throws RemoteException{
		ISorter sorter = new BubbleSorter();
		Comparable[] list = {100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199};
		long startTime = System.currentTimeMillis();
		for(int i = 0; i < sortTimes; i++){
			sorter.sort(list);
		}
		long finishTime = System.currentTimeMillis();
		
		System.out.println("BubbleSorter took 4x: "+(finishTime-startTime)+ " ms");
	}

	@Test
	public void preformenceSelectionSorterTest() throws RemoteException{
		ISorter sorter = new SelectionSorter();
		Comparable[] list = {100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199};
		long startTime = System.currentTimeMillis();
		for(int i = 0; i < sortTimes; i++){
			sorter.sort(list);
		}
		long finishTime = System.currentTimeMillis();
		
		System.out.println("SelectionSorter took: "+(finishTime-startTime)+ " ms");
	}

	@Test
	public void preformenceSelectionSorter2xTest() throws RemoteException{
		ISorter sorter = new SelectionSorter();
		Comparable[] list = {100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199};
		long startTime = System.currentTimeMillis();
		for(int i = 0; i < sortTimes; i++){
			sorter.sort(list);
		}
		long finishTime = System.currentTimeMillis();
		
		System.out.println("SelectionSorter took 2x: "+(finishTime-startTime)+ " ms");
	}

	@Test
	public void preformenceSelectionSorter4xTest() throws RemoteException{
		ISorter sorter = new SelectionSorter();
		Comparable[] list = {100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199,100,48,493,39,49,302,39,586,7,878,48,3,2,1,4,6,7,5,4,34,23,3,21,32,199};
		long startTime = System.currentTimeMillis();
		for(int i = 0; i < sortTimes; i++){
			sorter.sort(list);
		}
		long finishTime = System.currentTimeMillis();
		
		System.out.println("SelectionSorter took 4x: "+(finishTime-startTime)+ " ms");
	}
}
