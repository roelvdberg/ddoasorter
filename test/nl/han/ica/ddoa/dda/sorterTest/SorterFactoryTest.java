package nl.han.ica.ddoa.dda.sorterTest;

import static org.junit.Assert.*;

import java.rmi.RemoteException;

import nl.han.ica.ddoa.dda.interfaces.ISortFactory;
import nl.han.ica.ddoa.dda.interfaces.ISorter;
import nl.han.ica.ddoa.dda.sortServer.SortFactory;

import org.junit.Test;
import org.junit.Before;

public class SorterFactoryTest {
	private ISortFactory sortFactory;
	@Before
	public void tearUp(){
		try {
			this.sortFactory = SortFactory.getSortFactory();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void createSorterSingleTon() throws RemoteException{
		assertEquals(this.sortFactory,SortFactory.getSortFactory());
	}

}
